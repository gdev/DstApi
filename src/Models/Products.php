<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 20.3.19.
 * Time: 16.49
 */

namespace Gdev\Dst\Models;


class Products
{
    public $language;
    public $country;
    public $currency;
    public $identifiers;
    public $orderInformation;
    public $productClassification;
    public $productInformation;
    public $bikeSpecifics;
    public $eBikeSpecifics;
    public $relatedProducts;
    public $dateModified;

    public function __construct(string $language,string $country,string $currency,$identifiers,$orderInformation,$productClassification,$productInformation, $bikeSpecifics,$eBikeSpecifics,$relatedProducts,$dateModified)
    {
        $this->language = $language;
        $this->country = $country;
        $this->currency = $currency;
        $this->identifiers = $identifiers;
        $this->orderInformation = $orderInformation;
        $this->productClassification = $productClassification;
        $this->productInformation = $productInformation;
        $this->bikeSpecifics = $bikeSpecifics;
        $this->eBikeSpecifics = $eBikeSpecifics;
        $this->relatedProducts = $relatedProducts;
        $this->dateModified = $dateModified;
    }
}
