<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 20.3.19.
 * Time: 16.22
 */

namespace Gdev\Dst\Models;


class SupplierProductsResponse
{
    public $total;
    public $offset;
    public $products = [];
}
