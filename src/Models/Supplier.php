<?php
/**
 * Created by PhpStorm.
 * User: miske
 * Date: 20.3.19.
 * Time: 15.35
 */

namespace Gdev\Dst\Models;


class Supplier
{
    public $id;
    public $title;
    public $name;

    /**
     * Supplier constructor.
     * @param int    $id
     * @param string $title
     * @param string $name
     */
    public function __construct(int $id, string $title, string $name)
    {
        $this->id = $id;
        $this->title = $title;
        $this->name = $name;
    }
}
