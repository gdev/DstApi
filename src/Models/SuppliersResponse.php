<?php

namespace Gdev\Dst\Models;

/**
 * Class SuppliersResponse
 * @package Gdev\Dst\Models
 *
 * @property Supplier[] Supplier
 */
class SuppliersResponse
{
    public $suppliers = [];
}
