<?php
namespace Gdev\Dst;

use DateTime;
use Unirest\Response;
use Unirest\Request;

class DstAdapter
{
    private $username;
    private $password;
    private $headers = ['Accept' => 'application/json'];
    public const API_URL = 'http://api-v2.platformdst.nl';

    /**
     * DstAdapter constructor.
     * @param string $username
     * @param string $password
     */
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function getSuppliers(): Response
    {
        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/Suppliers";

        Request::auth($this->username, $this->password);
        $response = Request::get($url, $this->headers);

        return $response;
    }

    /**
     * @param string $type
     * @param string $parameter
     * @return Response
     */
    public function getProducts(string $type,string $parameter): Response
    {
        $apiUrl = static::API_URL;
        $url = "{$apiUrl}/Customers/Products";
        $params = [
            $type => $parameter
        ];
        Request::auth($this->username, $this->password);
        $response = Request::get($url,$this->headers,$params);


        return $response;
    }

}


