<?php

namespace Gdev\Dst;

use DateTime;
use Gdev\Dst\Models\Products;
use Gdev\Dst\Models\Supplier;
use Gdev\Dst\Models\SupplierProductsResponse;
use Gdev\Dst\Models\SuppliersResponse;
use Unirest\Response;

class DstObjectDecorator
{

    private $adapter;

    /**
     * DstObjectDecorator constructor.
     * @param DstAdapter $adapter
     */
    public function __construct(DstAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return SuppliersResponse
     */
    public function getSuppliers(): SuppliersResponse
    {
        $response = $this->adapter->getSuppliers();
        $result = new SuppliersResponse();
        foreach ($response->body as $supplier) {
            $result->suppliers[] = new Supplier($supplier->id, $supplier->title, $supplier->name);
        }
        return $result;
    }


    /**
     * @param string $type
     * @param string $parameter
     * @return SupplierProductsResponse

     */
    public function getProducts(string $type, string $parameter): SupplierProductsResponse
    {
        $response = $this->adapter->getProducts($type, $parameter);
        $result = new SupplierProductsResponse();
        $result->total = $response->body->total;
        $result->offset = $response->body->offset;

        foreach ($response->body->products as $product) {
            $result->products[] = new Products(
                $product->language,
                $product->country,
                $product->currency,
                $product->identifiers,
                $product->orderinformation,
                $product->productclassification,
                $product->productinformation,
                $product->bike_specifics,
                $product->ebike_specifics,
                $product->related_products,
                $product->dateModified
            );
        }
        return $result;
    }
}
